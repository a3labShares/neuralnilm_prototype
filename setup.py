from setuptools import setup, find_packages

MAJOR = 1
MINOR = 0
MICRO = 0
ISRELEASED = True
VERSION = '%d.%d.%d' % (MAJOR, MINOR, MICRO)
QUALIFIER = '-a3lab'

FULLVERSION = VERSION
FULLVERSION += QUALIFIER

setup(
    name='NeuralNILM',
    version=FULLVERSION,
    packages=find_packages()
)
