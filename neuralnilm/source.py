from __future__ import print_function, division
from queue import Queue, Empty
import threading
import numpy as np
import pandas as pd
from nilmtk import DataSet, TimeFrame, MeterGroup
from datetime import timedelta
from sys import stdout
from collections import OrderedDict
# from lasagne.utils import floatX
from keras import backend as K
from warnings import warn
import gc
from nilmtk.electric import activation_series_for_chunk
from nilmtk.timeframegroup import TimeFrameGroup
import logging
from .rectangulariser import rectangularise, start_and_end_and_mean
import os
import time
import pdb

SECS_PER_DAY = 60 * 60 * 24


class Batch(object):
    def __init__(self, data, target_power_timeseries, metadata=None):
        self.data = data
        self.target_power_timeseries = target_power_timeseries
        self.metadata = OrderedDict({} if metadata is None else metadata)


class Source(object):
    def __init__(self, seq_length, n_seq_per_batch, n_inputs, n_outputs,
                 logger=None,
                 X_processing_func=lambda X: X,
                 y_processing_func=lambda y: y,
                 reshape_target_to_2D=False,
                 independently_center_inputs=False,
                 standardise_input=False,
                 unit_variance_targets=False,
                 standardise_targets=False,
                 input_padding=0,
                 input_stats=None,
                 target_stats=None,
                 seed=42,
                 output_central_value=False,
                 classification=False,
                 random_window=0,
                 clock_period=None,
                 clock_type=None,
                 two_pass=False,
                 subsample_target=1,
                 n_rectangular_segments=None,
                 rectangular_kwargs=None,
                 target_is_start_and_end_and_mean=False,
                 nus_enabled=False,
                 nus_subsampling_ratio=10,
                 nus_expansion_ratio=10,
                 us_enabled=False,
                 us_subsampling_ratio=10):
        """
        Parameters
        ----------
        clock_type : {'one_hot', 'ramp'}
        """
        self._set_logger(logger)

        self.seq_length = seq_length
        self.n_seq_per_batch = n_seq_per_batch
        self.n_inputs = n_inputs
        self.n_outputs = n_outputs
        self.input_padding = input_padding
        self.queue = Queue(maxsize=2)
        self._stop = threading.Event()
        self._thread = None
        self.X_processing_func = X_processing_func
        self.y_processing_func = y_processing_func
        self.reshape_target_to_2D = reshape_target_to_2D
        self.rng = np.random.RandomState(seed)
        self.output_central_value = output_central_value
        self.classification = classification
        self.random_window = random_window
        self.subsample_target = subsample_target
        self.n_rectangular_segments = n_rectangular_segments
        self.rectangular_kwargs = (
            {} if rectangular_kwargs is None else rectangular_kwargs)
        self.target_is_start_and_end_and_mean = (
            target_is_start_and_end_and_mean)

        if self.seq_length % self.subsample_target:
            raise RuntimeError(
                "subsample_target must exactly divide seq_length.")

        if self.subsample_target == 0:
            raise RuntimeError("subsample_target must not be 0.")

        if self.random_window and self.subsample_target:
            if self.random_window % self.subsample_target:
                raise RuntimeError(
                    "subsample_target must exactly divide random_window")

        self.input_stats = input_stats
        self.target_stats = target_stats
        self.independently_center_inputs = independently_center_inputs
        self.standardise_input = standardise_input
        self.standardise_targets = standardise_targets
        self.unit_variance_targets = unit_variance_targets

        self.nus_enabled = nus_enabled
        self.nus_subsampling_ratio = nus_subsampling_ratio  # subsampling factor
        self.nus_expansion_ratio = nus_expansion_ratio  # dimension of the expansion window rescpet to sampling frequency

        self.us_enabled= us_enabled
        self.us_subsampling_ratio = us_subsampling_ratio

        self._init_data()
        self._initialise_standardisation()

        if 'lag' in self.__dict__:
            self.clock_period = (self.lag if clock_period is None
                                 else clock_period)
        self.clock_type = clock_type
        self.two_pass = two_pass

    def _set_logger(self, logger):
        if hasattr(self, 'logger'):
            return
        elif logger is None:
            self.logger = logging.getLogger(__name__)
            self.logger.setLevel(logging.DEBUG)
            if not self.logger.handlers:
                self.logger.addHandler(logging.StreamHandler(stdout))
        else:
            self.logger = logger

    def _init_data(self):
        pass

    def start(self):
        if self._thread is not None:
            return
        self._stop.clear()
        self._thread = threading.Thread(target=self.run)
        self._thread.start()

    def run(self):
        """Puts training data into a Queue"""
        while not self._stop.is_set():
            batch = self.get_batch()
            self.queue.put(batch)
        self.empty_queue()

    def _initialise_standardisation(self):
        if not (self.standardise_input or self.standardise_targets):
            return

        if self.input_stats is None:
            X, y = self._gen_data()
            X = X.reshape(
                self.n_seq_per_batch * (self.seq_length + self.input_padding),
                self.n_inputs)
            self.in_stats = {'mean': X.mean(axis=0), 'std': X.std(axis=0)}
            self.input_stats = {'mean': [], 'std': []}
            for input_i in self.n_inputs:
                self.input_stats['mean'].append(self.in_stats['mean'][input_i])
                self.input_stats['std'].append(self.in_stats['std'][input_i])

        if self.target_stats is None:
            # Get targets.  Temporarily turn off skip probability
            if 'skip_probability' in self.__dict__:
                skip_prob = self.skip_probability
                self.skip_probability = 0
            if 'skip_probability_for_first_appliance' in self.__dict__:
                skip_prob_for_first_appliance = (
                    self.skip_probability_for_first_appliance)
                self.skip_probability_for_first_appliance = 0
            X, y = self._gen_data()
            if 'skip_probability' in self.__dict__:
                self.skip_probability = skip_prob
            if 'skip_probability_for_first_appliance' in self.__dict__:
                self.skip_probability_for_first_appliance = (
                    skip_prob_for_first_appliance)

            y = y.reshape(self.n_seq_per_batch * self.seq_length,
                          self.n_outputs)
            self.target_stats = {'mean': y.mean(axis=0), 'std': y.std(axis=0)}

        np.save('target_stats_mean', self.target_stats['mean'])
        np.save('target_stats_std', self.target_stats['std'])
        np.save('input_stats_mean', self.input_stats['mean'])
        np.save('input_stats_std', self.input_stats['std'])

    def stop(self):
        self.empty_queue()
        self._stop.set()
        if self._thread is not None:
            self._thread.join()
            self._thread = None

    def get(self, timeout=30, **kwargs):
        if self._thread is None:
            self.start()
        return self.queue.get(timeout=timeout, **kwargs)

    def empty_queue(self):
        while True:
            try:
                self.queue.get(block=False)
            except Empty:
                break

    def validation_data(self, valid_ratio=0.2):
        #return self.get_batch(validation=False)
        return self.get_batch(validation=True, valid_ratio=valid_ratio)

    def get_batch(self, validation=False, valid_ratio=0.2, standardize=True):
        x, y = self._gen_data(validation=validation, valid_ratio=valid_ratio)
        if standardize:
            x, y = self._process_data(x, y)
        data = (x, y)
        batch = Batch(data=data, target_power_timeseries=y)
        return batch

    def _process_data(self, X, y):
        if self.subsample_target > 1:
            y = y.reshape(self.n_seq_per_batch, -1,
                          self.subsample_target, self.n_outputs)
            y = y.mean(axis=2)

        if self.input_padding:
            pad = self.input_padding // 2

            X = np.pad(X, pad_width=((0, 0), (pad, pad), (0, 0)), mode='constant')

        def _standardise(n, stats, data):
            for i in range(n):
                mean = stats['mean'][i]
                std = stats['std'][i]
                data[:, :, i] = standardise(data[:, :, i], how='std=1', mean=mean, std=std)
            return data

        if self.independently_center_inputs:
            for seq_i in range(self.n_seq_per_batch):
                for input_i in range(self.n_inputs):
                    X[seq_i, :, input_i] = standardise(X[seq_i, :, input_i],
                                                       how='std=1',
                                                       std=self.input_stats['std'][input_i])

        elif self.standardise_input:
            X = _standardise(self.n_inputs, self.input_stats, X)

        if self.unit_variance_targets:
            for i in range(self.n_outputs):
                std = self.target_stats['std'][i]
                y[:, :, i] /= std
        elif self.standardise_targets:
            y = _standardise(self.n_outputs, self.target_stats, y)

        if self.random_window:
            y_seq_length = self.seq_length // self.subsample_target
            y_window_width = self.random_window // self.subsample_target
            latest_y_window_start = y_seq_length - y_window_width
            y_window_start = self.rng.randint(0, latest_y_window_start)
            y_window_end = y_window_start + y_window_width
            y = y[:, y_window_start:y_window_end, :]
            x_window_start = y_window_start * self.subsample_target
            x_window_end = y_window_end * self.subsample_target
            X = X[:, x_window_start:x_window_end, :]

        if self.reshape_target_to_2D:
            y = y.reshape(self.output_shape_after_processing())

        X = self.X_processing_func(X)
        y = self.y_processing_func(y)
        if self.n_rectangular_segments is not None:
            y = rectangularise(y, n_segments=self.n_rectangular_segments, **self.rectangular_kwargs)

        if self.target_is_start_and_end_and_mean:
            y = start_and_end_and_mean(y)

        if self.classification:
            y = (y > 0).max(axis=1).reshape(
                self.output_shape_after_processing())
        elif self.output_central_value:
            seq_length = y.shape[1]
            half_seq_length = seq_length // 2
            y = y[:, half_seq_length:half_seq_length + 1, :]

        if self.clock_type == 'one_hot':
            clock = np.zeros(shape=(self.n_seq_per_batch, self.seq_length, self.n_inputs), dtype=np.float32)

            for i in range(self.clock_period):
                clock[:, i::self.clock_period, i] = 1

        elif self.clock_type == 'ramp':
            ramp = np.linspace(start=-1, stop=1, num=self.clock_period, dtype=np.float32)
            n_ramps = np.ceil(self.seq_length / self.clock_period)
            ramp_for_one_seq = np.tile(ramp, n_ramps)[:self.seq_length]
            clock = np.tile(ramp_for_one_seq, (self.n_seq_per_batch, 1))
            clock = clock.reshape((self.n_seq_per_batch, self.seq_length, 1))

        if self.clock_type is not None:
            X = np.concatenate((X, clock), axis=2)

        if self.two_pass:
            X = np.tile(X, (1, 2, 1))
            shape = (self.n_seq_per_batch, self.seq_length * 2, 1)
            encode_flag = np.zeros(shape)
            encode_flag[:, :self.seq_length, :] = 0
            encode_flag[:, self.seq_length:, :] = 1
            X = np.concatenate((X, encode_flag), axis=2)
            X[:, self.seq_length:, 0] = 0
            y = np.tile(y, (1, 2, 1))

        # X, y = floatX(X), floatX(y)
        X, y = K.cast_to_floatx(X), K.cast_to_floatx(y)
        self._check_data(X, y)
        return X, y

    def _gen_data(self, validation=False, valid_ratio=0.2):
        raise NotImplementedError()

    def input_shape(self):  # -------------------
        if self.random_window:
            seq_length = self.random_window
        else:
            seq_length = self.seq_length

        return (self.n_seq_per_batch,
                seq_length,
                self.n_inputs)

    def input_shape_after_processing(self):
        n_seq_per_batch, seq_length, n_inputs = self.input_shape()
        seq_length += self.input_padding
        if self.random_window:
            seq_length = self.random_window
        if self.two_pass:
            seq_length *= 2
        if self.clock_type == 'one_hot':
            n_inputs += self.clock_period
        elif self.clock_type == 'ramp':
            n_inputs += 1
        if self.two_pass:
            n_inputs += 1
        return (n_seq_per_batch, seq_length, n_inputs)

    def output_shape(self):
        return (self.n_seq_per_batch, self.seq_length, self.n_outputs)

    def output_shape_after_processing(self):
        n_seq_per_batch, seq_length, n_outputs = self.output_shape()
        if self.random_window:
            seq_length = self.random_window
        if self.two_pass:
            seq_length *= 2
        if self.subsample_target > 1:
            seq_length = seq_length // self.subsample_target
        if self.n_rectangular_segments:
            rectangular_format = self.rectangular_kwargs.get(
                'format', 'proportional')
            if rectangular_format == 'proportional':
                seq_length = self.n_rectangular_segments
            else:
                seq_length = self.n_rectangular_segments - 1
            n_outputs = 1
        if self.target_is_start_and_end_and_mean:
            seq_length = 3

        if self.reshape_target_to_2D:
            return (n_seq_per_batch * seq_length, n_outputs)
        elif self.output_central_value or self.classification:
            return (n_seq_per_batch, 1, n_outputs)
        else:
            return (n_seq_per_batch, seq_length, n_outputs)

    def _check_data(self, X, y):
        assert X.shape == self.input_shape_after_processing()
        assert not any(np.isnan(X.flatten()))
        if y is not None:
            assert y.shape == self.output_shape_after_processing()
            assert not any(np.isnan(y.flatten()))


def none_to_list(x):
    return [] if x is None else x


class ToySource(Source):
    def __init__(self, seq_length, n_seq_per_batch, n_inputs=1,
                 powers=None, on_durations=None, all_hot=True,
                 fdiff=False):
        """
        Parameters
        ----------
        n_inputs : int
            if > 1 then will quantize inputs
        powers : list of numbers
        on_durations : list of numbers
        """
        self.powers = [10, 40] if powers is None else powers
        self.on_durations = [3, 10] if on_durations is None else on_durations
        self.all_hot = all_hot
        self.fdiff = fdiff
        super(ToySource, self).__init__(
            seq_length=seq_length,
            n_seq_per_batch=n_seq_per_batch,
            n_inputs=n_inputs,
            n_outputs=1)

    def _gen_single_appliance(self, power, on_duration,
                              min_off_duration=20, p=0.2):
        length = self.seq_length + 1 if self.fdiff else self.seq_length
        appliance_power = np.zeros(length)
        i = 0
        while i < length:
            if self.rng.binomial(n=1, p=p):
                end = min(i + on_duration, length)
                appliance_power[i:end] = power
                i += on_duration + min_off_duration
            else:
                i += 1
        return np.diff(appliance_power) if self.fdiff else appliance_power

    def _gen_batches_of_single_appliance(self, *args, **kwargs):
        batches = np.empty(shape=(self.n_seq_per_batch, self.seq_length, 1))
        for i in range(self.n_seq_per_batch):
            single_appliance = self._gen_single_appliance(*args, **kwargs)
            batches[i, :, :] = single_appliance.reshape(self.seq_length, 1)
        return batches

    def _gen_unquantized_data(self, validation=False):
        y = self._gen_batches_of_single_appliance(
            power=self.powers[0], on_duration=self.on_durations[0])
        X = y.copy()
        for power, on_duration in zip(self.powers, self.on_durations)[1:]:
            X += self._gen_batches_of_single_appliance(
                power=power, on_duration=on_duration)

        return X / np.max(X), y / np.max(y)

    def _gen_data(self, *args, **kwargs):
        X, y = self._gen_unquantized_data(*args, **kwargs)
        if self.n_inputs > 1:
            X = quantize(X, self.n_inputs, self.all_hot)
        return X, y


class RealApplianceSource(Source):
    def __init__(self, filename, appliances,
                 min_on_durations,
                 logger=None,
                 max_appliance_powers=None,
                 min_off_durations=None,
                 on_power_thresholds=None,
                 max_input_power=None,
                 divide_input_by_max_input_power=True,
                 clip_input=True,
                 window=(None, None),
                 seq_length=1000,
                 n_seq_per_batch=5,
                 train_buildings=None,
                 validation_buildings=None,
                 output_one_appliance=True,
                 sample_period=6,
                 boolean_targets=False,
                 skip_probability=0,
                 skip_probability_for_first_appliance=None,
                 include_diff=False,
                 include_power=True,
                 target_is_diff=False,
                 max_diff=3000,
                 clip_appliance_power=True,
                 lag=None,
                 target_is_prediction=False,
                 one_target_per_seq=False,
                 ensure_all_appliances_represented=True,
                 border=5,
                 window_per_building=None,
                 **kwargs):
        """
        Parameters
        ----------
        filename : str
        appliances : list of strings
            The first one is the target appliance, if
            output_one_appliance is True.
            Use a list of lists for alternative names
            (e.g. ['fridge', 'fridge freezer']).
        subsample_target : int
            If > 1 then subsample the targets.
        skip_probability : float, [0, 1]
            If `skip_probability` == 0 then all appliances will be included in
            every sequence.  Else each appliance will be skipped with this
            probability but every appliance will be present in at least
            one sequence per batch.
        window_per_building : dict
            Keys are Building instance integers.
            Must have one for each building.
            Values are (<start>, <end>) dates (or whatever 
            nilmtk.DataSet.set_window() accepts)
        """
        self._set_logger(logger)
        self.dataset = DataSet(filename)
        self.filename = filename
        self.appliances = appliances

        if max_input_power is None and max_appliance_powers is not None:
            self.max_input_power = np.sum(max_appliance_powers)
        else:
            self.max_input_power = max_input_power
        self.clip_input = clip_input

        self.divide_input_by_max_input_power = divide_input_by_max_input_power
        if max_appliance_powers is None:
            max_appliance_powers = [None] * len(appliances)

        self.max_appliance_powers = OrderedDict()
        for i, appliance in enumerate(appliances):
            if isinstance(appliance, list):
                appliance_name = appliance[0]
            else:
                appliance_name = appliance
            self.max_appliance_powers[appliance_name] = max_appliance_powers[i]

        self.dataset.set_window(*window)
        if train_buildings is None:
            train_buildings = [1]
        if validation_buildings is None:
            validation_buildings = [1]

        self.validation_buildings = validation_buildings
        self.train_buildings = train_buildings

        self.output_one_appliance = output_one_appliance
        self.sample_period = sample_period
        self.boolean_targets = boolean_targets
        self.skip_probability = skip_probability
        if skip_probability_for_first_appliance is None:
            self.skip_probability_for_first_appliance = skip_probability
        else:
            self.skip_probability_for_first_appliance = (
                skip_probability_for_first_appliance)
        self._tz = self.dataset.metadata['timezone']
        self.include_diff = include_diff
        self.include_power = include_power
        self.max_diff = max_diff
        self.clip_appliance_power = clip_appliance_power
        if lag is None:
            lag = -1 if target_is_prediction else 0
        elif lag == 0 and target_is_prediction:
            warn("lag is 0 and target_is_prediction==True."
                 "  Hence output will be identical to input.")
        self.lag = lag
        self.target_is_prediction = target_is_prediction
        self.target_is_diff = target_is_diff
        self.one_target_per_seq = one_target_per_seq
        self.ensure_all_appliances_represented = (ensure_all_appliances_represented)
        self.border = border
        if window_per_building and window != (None, None):
            raise ValueError("Cannot set both `window_per_building` and `window`")
        self.window_per_building = window_per_building

        self.logger.info("Loading training activations...")
        if on_power_thresholds is None:
            on_power_thresholds = [None] * len(self.appliances)
        self.on_power_thresholds = on_power_thresholds
        if min_on_durations is None:
            min_on_durations = [0] * len(self.appliances)

        super(RealApplianceSource, self).__init__(seq_length=seq_length,
                                                  n_seq_per_batch=n_seq_per_batch,
                                                  n_outputs=(1 if output_one_appliance or target_is_prediction
                                                             else len(appliances)),
                                                  **kwargs)

        self.train_activations, self.train_r_activations = self._load_activations(train_buildings,
                                                                                  min_on_durations,
                                                                                  min_off_durations,
                                                                                  on_power_thresholds,
                                                                                  kwargs['n_inputs'])

        # if same training nad validation data (always used in the current experiments)
        if train_buildings == validation_buildings:
            self.validation_activations = {}
            # Remove or not, from train_activations, the activations that are used for validation are relevant only if it is set as source_type in r602.py,
            # the string 'real_appliance_source', that is only if is is used the synthetic data. In fact, if both real and synthetic data are used, only the
            # real ones are adopted in the validation process (moreover, no synthetic data are used in test).

            # validation activations will be removed from train_activations
            # self.remove_used_activations = True
            self.remove_used_activations = False
        else:
            self.logger.info("Loading validation activations...")
            self.remove_used_activations = False
            self.validation_activations, self.validation_r_activations = self._load_activations(
                validation_buildings, min_on_durations, min_off_durations,
                on_power_thresholds)
            self.dataset.store.close()

        if kwargs['n_inputs'] != 2 and kwargs['n_inputs'] != 1:
            kwargs['n_inputs'] = sum([include_diff, include_power])

        # super(RealApplianceSource, self).__init__(seq_length=seq_length,
        #                                           n_seq_per_batch=n_seq_per_batch,
        #                                           n_outputs=(1 if output_one_appliance or target_is_prediction
        #                                                      else len(appliances)),
        #                                           **kwargs)

        assert not (self.input_padding and self.random_window)
        self.logger.info("Done loading activations.")

    def get_labels(self, building=1):
        return self.train_activations[building].keys()

    def _load_activations(self, buildings, min_on_durations, min_off_durations, on_power_thresholds, n_inputs):

        activations = OrderedDict()
        reactive_in_activations = OrderedDict()

        if self.nus_enabled:
            nus_labels = OrderedDict()
            self.logger.info("Subsampling activations to apply NUS")

        # per ogni casa (nel nostro caso e' unica)
        for building_i in buildings:

            activations[building_i] = OrderedDict()
            reactive_in_activations[building_i] = OrderedDict()

            # window_per_building fornito da r602.py
            if self.window_per_building:
                window = self.window_per_building[building_i]
                self.logger.info("Setting window for building {} to (start={}, end={})".format(building_i, *window))
                self.dataset.set_window(*window)
            elec = self.dataset.buildings[building_i].elec
            meters = get_meters_for_appliances(
                elec, self.appliances, self.logger)

            if self.nus_enabled:
                nus_labels[building_i] = dict()

                #
            #     subsampled_labels = pd.Series(0, index=pd.date_range(start=window[0], end=window[1], freq='{}S'.format(self.sample_period),tz=self._tz))
            #
            #     # set to 1 the samples at subsample ratio
            #     subsampled_ratio_indexes = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(self.sample_period * self.nus_subsampling_ratio),tz=self._tz)
            #     subsampled_labels[subsampled_ratio_indexes] = 1
            #
            #     # load ground truth for each appliance and specific building
            #     for appl in self.appliances:
            #         csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(self.filename))[0], building_i, appl.replace(' ', '_'))
            #
            #         # avoid ambigous error for date near dst
            #         appliance_ground_truth = pd.Series.from_csv(csv_filename).tz_localize('UTC')
            #         w0 = pd.Timestamp(window[0]).tz_localize(tz=self._tz).tz_convert('UTC')
            #         w1 = pd.Timestamp(window[1]).tz_localize(tz=self._tz).tz_convert('UTC')
            #         appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < w0], inplace=True)
            #         appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > w1], inplace=True)
            #         appliance_ground_truth = appliance_ground_truth.tz_convert(tz=self._tz)
            #
            #         for each in range(-self.nus_expansion_ratio//2,self.nus_expansion_ratio//2):
            #             subsampled_labels[appliance_ground_truth.index + timedelta(seconds=each * self.sample_period)] = 1
            #
            #     nus_labels[building_i] = subsampled_labels

            for appliance_i, meter in enumerate(meters):

                appliance = self.appliances[appliance_i]
                if isinstance(appliance, list):
                    appliance = appliance[0]
                # if isinstance(appliance, list):
                #     if appliance[0] in ['fridge freezer', 'fridge', 'freezer']:
                #         appliance = 'fridge'
                #     elif appliance[0] in ['washer dryer', 'washing machine']:
                #         appliance = 'washing machine'
                #     else:
                #         raise TypeError('Unrecognize appliance.')

                self.logger.info("Loading activations for {} from building {}...".format(appliance, building_i))
                if self.nus_enabled:
                    # activation must include enough samples around the state-change for the expansion
                    activation_series = meter.activation_series(
                        on_power_threshold=on_power_thresholds[appliance_i],
                        min_on_duration=min_on_durations[appliance_i],
                        min_off_duration=min_off_durations[appliance_i],
                        resample=True, physical_quantity='power', ac_type='active',
                        border=self.nus_expansion_ratio // 2)
                elif self.us_enabled:
                    # activation must include enough samples around the state-change to get at least one samples before and after the change state in the uniform subsampling
                    activation_series = meter.activation_series(
                        on_power_threshold=on_power_thresholds[appliance_i],
                        min_on_duration=min_on_durations[appliance_i],
                        min_off_duration=min_off_durations[appliance_i],
                        resample=True, physical_quantity='power', ac_type='active',
                        border=self.us_subsampling_ratio)
                else:
                    activation_series = meter.activation_series(
                        on_power_threshold=on_power_thresholds[appliance_i],
                        min_on_duration=min_on_durations[appliance_i],
                        min_off_duration=min_off_durations[appliance_i],
                        resample=True, physical_quantity='power', ac_type='active')

                activations[building_i][appliance] = _preprocess_activations(
                    activation_series,
                    max_power=self.max_appliance_powers[appliance],
                    sample_period=self.sample_period,
                    clip_appliance_power=self.clip_appliance_power)

                if self.nus_enabled:
                    # just load and store appliance ground truth (without subsampled_labels and expansions), to be used to generate batches
                    # leave activations intact in this stage

                    tmp_dataset_name = self.dataset.metadata['name'].lower().replace('-', '')
                    tmp_csv_dir = os.path.dirname(os.path.dirname(os.path.join(self.filename)))

                    # fix appliance name to select ground truth file
                    if appliance in ['fridge freezer', 'fridge', 'freezer']:
                        app = 'fridge'
                    elif tmp_dataset_name in ['ukdale', 'ampds'] and appliance in ['washer dryer', 'washing machine']:
                        app = 'washing_machine'
                    elif tmp_dataset_name == 'redd':
                        pass

                    else:
                        raise TypeError('Unrecognize appliance in selecting ground truth.')

                    # csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(self.filename))[0], building_i, appliance.replace(' ', '_'))
                    if __debug__:
                        csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(self.filename))[0], building_i, app.replace(' ', '_'))
                    else:
                        tmp_dataset_name = self.dataset.metadata['name'].lower().replace('-', '')
                        tmp_csv_dir = os.path.dirname(os.path.dirname(self.filename))
                        csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.join(tmp_csv_dir, tmp_dataset_name), building_i,  app.replace(' ', '_'))


                    #csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.join(tmp_csv_dir, tmp_dataset_name), building_i,  appliance.replace(' ', '_'))

                    # avoid ambigous error for date near dst
                    appliance_ground_truth = pd.Series.from_csv(csv_filename).tz_localize('UTC')
                    w0 = pd.Timestamp(window[0]).tz_localize(tz=self._tz).tz_convert('UTC')
                    w1 = pd.Timestamp(window[1]).tz_localize(tz=self._tz).tz_convert('UTC')
                    appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < w0], inplace=True)
                    appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > w1], inplace=True)
                    nus_labels[building_i][appliance] = appliance_ground_truth.tz_convert(tz=self._tz)

                # if self.nus_enabled:
                #
                #     # filter activation
                #     for index, activation in enumerate(activations[appliance]):
                #         try:
                #             # labels to drop
                #             labels = subsampled_labels[subsampled_labels == 0].index
                #             labels = labels[(labels >= activation.index[0]) & (labels <= activation.index[-1])]
                #             if not len(labels):
                #                 # do avoid strage errors, drop labels only if exist
                #                 # print("Activation length after subsampling: {}".format(len(activation)))
                #                 continue
                #
                #             # activation.drop(labels=labels, inplace=True)
                #             activations[appliance][index] = activation.tz_convert('UTC').drop(labels=labels.tz_convert('UTC')).tz_convert(self._tz)
                #
                #             if not activation.any():
                #                 self.logger.warning("Activation series subsampled to empty series.")
                #
                #         except:
                #             self.logger.exception("NUS - unhandled error in activations subsampling:\n")
                #             raise Exception

                if n_inputs == 2:
                    # mette in reactive_p i campioni di potenza (la serie) della appliance considerata (vedi meter)
                    reactive_p = meter.power_series_all_data(sample_period=self.sample_period,
                                                             physical_quantity='power',
                                                             ac_type='reactive')
                    r_activation_series = []
                    for n_act in range(0, len(activation_series)):
                        start_r = TimeFrame(activation_series[n_act].index[0], activation_series[n_act].index[-1]).start
                        end_r = TimeFrame(activation_series[n_act].index[0], activation_series[n_act].index[-1]).end
                        # mette in reactive_in_n_act i campioni di potenza reattiva relativi all'intervallo di tempo
                        # in cui avviene l'attivazione n_act
                        reactive_in_n_act = reactive_p[start_r:end_r]
                        # mette in r_activation_series una lista con le serie di potenza reattiva corrispondenti alle
                        # attivazioni dell'appliance considerata
                        r_activation_series.append(reactive_in_n_act)

                        if len(activation_series[n_act]) != len(reactive_in_n_act):
                            print('dimensioni non uguali')
                            print(appliance)
                            print(n_act)

                    reactive_in_activations[building_i][appliance] = _preprocess_activations(r_activation_series,
                                                                                             max_power=self.max_appliance_powers[
                                                                                                 appliance],
                                                                                             sample_period=self.sample_period,
                                                                                             clip_appliance_power=self.clip_appliance_power)
                self.logger.info("Loaded {:d} activations.".format(len(activation_series)))

        if self.nus_enabled:
            self.nus_labels = nus_labels

        return activations, reactive_in_activations

    """    
    def _load_r_activations(self, activations):
        r_activations = OrderedDict()
        for building_i in buildings:
            if self.window_per_building:
                window = self.window_per_building[building_i]
                self.logger.info(
                    "Setting window for building {} to (start={}, end={})"
                    .format(building_i, *window))
                self.dataset.set_window(*window)
            elec = self.dataset.buildings[building_i].elec
            meters = get_meters_for_appliances(
                elec, self.appliances, self.logger)
            for appliance_i, meter in enumerate(meters):
                appliance = self.appliances[appliance_i]
                if isinstance(appliance, list):
                    appliance = appliance[0]
                self.logger.info(
                    "  Loading r_activations for {} from building {}..."

                #for i : len
                    activation_series= 
                    r_activations[appliance] = _preprocess_activations(
                        activation_series,
                        max_power=self.max_appliance_powers[appliance],
                        sample_period=self.sample_period,
                        clip_appliance_power=self.clip_appliance_power)   #### my_mod ####
                    self.logger.info(
                        "    Loaded {:d} activations."
                        .format(len(activation_series)))
        return r_activations
        """

    def _gen_single_example(self, validation=False, appliances=None):

        # Use fixed bulding
        # building_i = 1
        # Rnd select a building
        buildings = (self.validation_buildings if validation else self.train_buildings)
        building_i = self.rng.choice(buildings, 1)[0]

        if self.nus_enabled:
            X_shifts = dict()

        if appliances is None:
            appliances = []

        X = np.zeros(shape=(self.seq_length, self.n_inputs), dtype=np.float32)
        y = np.zeros(shape=(self.seq_length, self.n_outputs), dtype=np.float32)
        if self.nus_enabled:
            nus_labels = np.zeros(shape=self.seq_length, dtype=np.bool)

        POWER_THRESHOLD = 1
        if validation and self.validation_activations:
            activations = self.validation_activations[building_i]
            r_activations = self.validation_r_activations[building_i]
        else:
            activations = self.train_activations[building_i]
            r_activations = self.train_r_activations[building_i]

        if not self.one_target_per_seq:
            random_appliances = []
            appliance_names = activations.keys()
            while not random_appliances:
                if not self.rng.binomial(n=1, p=self.skip_probability_for_first_appliance):
                    appliance_i = 0
                    appliance = appliance_names[0]
                    random_appliances.append((appliance_i, appliance))
                for appliance_i, appliance in enumerate(appliance_names[1:]):
                    if not self.rng.binomial(n=1, p=self.skip_probability):
                        random_appliances.append((appliance_i + 1, appliance))

            appliances.extend(random_appliances)

        appliances = list(set(appliances))  # make unique
        # for each evaluated appliance
        for appliance_i, appliance in appliances:

            # fix appliance with multiple name
            # if appliance in ['fridge freezer', 'fridge', 'freezer']:
            #     appliance = 'fridge'
            # elif appliance in ['washer dryer', 'washing machine']:
            #     appliance = 'washing machine'
            if isinstance(appliance, list):
                appliance = appliance[0]

            n_activations = len(activations[appliance])
            if n_activations == 0:
                continue
            # slec a random activation between n_activations of the evaluated appliance
            activation_i = self.rng.randint(0, n_activations)
            if self.remove_used_activations and validation:
                activation = activations[appliance].pop(activation_i)
                if self.n_inputs == 2:
                    r_activation = r_activations[appliance].pop(activation_i)
            else:
                activation = activations[appliance][activation_i]
                if self.n_inputs == 2:
                    r_activation = r_activations[appliance][activation_i]
            if self.output_one_appliance and appliance_i > 0:
                # Allow appliance to start before the start of the target seq
                # and end after the end of the target seq
                latest_start_i = self.seq_length - (self.border + self.lag)
                earliest_start_i = self.border - len(activation)
            else:
                # Try to fit the appliance into the target seq
                latest_start_i = ((self.seq_length - len(activation)) -
                                  (self.border + self.lag))
                latest_start_i = max(latest_start_i, self.border)
                earliest_start_i = 0
            start_i = self.rng.randint(earliest_start_i, latest_start_i)
            X_start_i = max(0, start_i)
            activation_start_i = -min(0, start_i)
            activation_end_i = (activation_start_i + self.seq_length - X_start_i - self.lag)

            target = activation.values[activation_start_i:activation_end_i]
            if self.n_inputs == 2:
                r_target = r_activation.values[activation_start_i:activation_end_i]

            X_end_i = X_start_i + len(target)
            assert X_end_i <= self.seq_length

            X[X_start_i:X_end_i, 0] += target

            if self.n_inputs == 2:
                X[X_start_i:X_end_i, 1] += r_target

            if self.nus_enabled:
                # For each appliance select the nus_labels portion, and finally create the nus labels for the given appliances and apply to the X and y
                def generate_nus_labels(activation, building_i, appliance):

                    subsampled_labels = pd.Series(0, index=pd.date_range(start=activation.index[0], end=activation.index[-1], freq='{}S'.format(self.sample_period)),
                                                  dtype=np.bool)

                    datatime_indices = self.nus_labels[building_i][appliance][activation.index[0]:activation.index[-1]].index
                    for each in range(-self.nus_expansion_ratio//2,self.nus_expansion_ratio//2):
                        datatime_indices = datatime_indices.union(self.nus_labels[building_i][appliance][activation.index[0]:activation.index[-1]].index + timedelta(seconds=each * self.sample_period))

                    # set to True NUS points and using datatime_indices bewteen the subsampled_labels range
                    subsampled_labels[datatime_indices[(activation.index[0] <= datatime_indices) & (datatime_indices <= activation.index[-1])]] = True

                    return subsampled_labels.values

                target_nus_labels = generate_nus_labels(activation[activation_start_i:activation_end_i], building_i, appliance)

                if len(target) != len(target_nus_labels):
                    raise ValueError("Target and NUS labels must have same length.")

                nus_labels[X_start_i:X_end_i] += target_nus_labels

            if (not self.target_is_prediction and (appliance_i == 0 or not self.output_one_appliance)):
                target = np.copy(target)
                if self.boolean_targets:
                    target[target <= POWER_THRESHOLD] = 0
                    target[target > POWER_THRESHOLD] = 1
                else:
                    max_appliance_power = self.max_appliance_powers[appliance]
                    if max_appliance_power is not None:
                        target /= max_appliance_power
                if self.target_is_diff:
                    y[(X_start_i + self.lag):(X_end_i + self.lag - 1), appliance_i] = np.diff(target)
                else:
                    y[(X_start_i + self.lag):(X_end_i + self.lag), appliance_i] = target

        if self.clip_input:
            np.clip(X, 0, self.max_input_power, out=X)

        fdiff = np.diff(X[:, 0]) / self.max_diff
        if self.divide_input_by_max_input_power and self.max_input_power is not None:
            X[:, 0] /= self.max_input_power

        if self.target_is_prediction:
            if self.target_is_diff:
                data = np.concatenate([fdiff, [0]]).reshape(self.seq_length, 1)
            else:
                data = np.copy(X)

            if self.lag > 0:
                y[self.lag:, :] = data[:-self.lag, :]
            elif self.lag == 0:
                y = data
            else:
                y[:self.lag, :] = data[-self.lag:, :]

        if self.include_diff:
            feature_i = int(self.include_power)
            X[:-1, feature_i] = fdiff

        if self.nus_enabled:
            # add constant subsampling to nus_labels and drop the X and y data, then pad to seq_length
            subsampled_vector = np.append([True], np.zeros(self.nus_subsampling_ratio-1, dtype=np.bool))
            subsampled_vector = np.tile(subsampled_vector, len(nus_labels) // self.nus_subsampling_ratio)

            if len(subsampled_vector) != len(nus_labels):
                # fill subsampled_vector
                diff = len(nus_labels) - len(subsampled_vector)
                vfill = np.append([True], np.zeros(diff-1, dtype=np.bool))
                subsampled_vector = np.append(subsampled_vector, vfill)

            nus_labels += subsampled_vector

            # filter X and y
            X = X[nus_labels]
            y = y[nus_labels]
            # pass with zeros
            if len(y) < self.seq_length:
                padding = (self.seq_length - len(y)) // 2
                X = np.pad(X, ((padding,), (0,)), 'constant')
                y = np.pad(y, ((padding,), (0,)), 'constant')
                if (self.seq_length - len(y)) % 2:
                    # add one zero at the end
                    X = np.pad(X, ((0,1), (0,0)), 'constant')
                    y = np.pad(y, ((0,1), (0,0)), 'constant')

        return X, y

    def _appliances_for_sequence(self):
        """Returns a dict which maps from seq_i to a list of appliances which
        must be included in that sequence.  This is used to ensure that,
        if `skip_probability` > 0 then every appliance must be represented in
        at least one sequence.
        """
        if not self.ensure_all_appliances_represented:
            return {}
        all_appliances = list(enumerate(self.get_labels()))
        if self.one_target_per_seq:
            return {i: [all_appliances[i % len(all_appliances)]]
                    for i in range(self.n_seq_per_batch)}
        if self.skip_probability == 0:
            return {i: [] for i in range(self.n_seq_per_batch)}
        n_appliances = len(self.appliances)
        n_appliances_per_seq = n_appliances // self.n_seq_per_batch
        remainder = n_appliances % self.n_seq_per_batch
        appliances_for_sequence = {}

        for i in range(self.n_seq_per_batch):
            start = n_appliances_per_seq * i
            end = start + n_appliances_per_seq
            if remainder:
                end += 1
                remainder -= 1
            appliances = all_appliances[start:end]
            appliances_for_sequence[i] = appliances
        return appliances_for_sequence

    def _gen_data(self, validation=False, valid_ratio=0.2):
        X = np.zeros(self.input_shape(), dtype=np.float32)
        y = np.zeros(self.output_shape(), dtype=np.float32)
        deterministic_appliances = self._appliances_for_sequence()

        for i in range(self.n_seq_per_batch):
            X[i, :, :], y[i, :, :] = self._gen_single_example(validation, deterministic_appliances.get(i))

        if self.remove_used_activations and validation:
            for appliance in self.appliances:
                if isinstance(appliance, list):
                    appliance = appliance[0]
                self.logger.info(
                    "{}: {:d} train activations".format(
                        appliance, len(self.train_activations[appliance])))  # TODO: self.train_activations add bulding_i
        return X, y


class NILMTKSource(Source):
    def __init__(self, filename, appliances,
                 train_buildings, validation_buildings,
                 window=(None, None),
                 sample_period=6,
                 **kwargs):
        super(NILMTKSource, self).__init__(
            n_outputs=len(appliances),
            n_inputs=1,
            **kwargs)
        self.window = window
        self.filename = filename
        self.dataset = DataSet(filename)
        self.dataset.set_window(*window)
        self.tz = self.dataset.metadata['timezone']
        self.window = [pd.Timestamp(ts, tz=self.tz) for ts in self.window]
        self.appliances = appliances
        self.train_buildings = train_buildings
        self.validation_buildings = validation_buildings
        self.sample_period = sample_period
        self._init_meter_groups()
        self._init_good_sections()

    def _init_meter_groups(self):
        self.metergroups = {}
        for building_i in self._all_buildings():
            elec = self.dataset.buildings[building_i].elec
            meters = get_meters_for_appliances(
                elec, self.appliances, self.logger)
            self.metergroups[building_i] = MeterGroup(meters)

    def _all_buildings(self):
        buildings = self.train_buildings + self.validation_buildings
        buildings = list(set(buildings))
        return buildings

    def _init_good_sections(self):
        self.good_sections = {}
        min_duration_secs = self.sample_period * self.seq_length
        min_duration = timedelta(seconds=min_duration_secs)
        for building_i in self._all_buildings():
            self.logger.info(
                "init good sections for building {}".format(building_i))
            mains = self.dataset.buildings[building_i].elec.mains()
            self.good_sections[building_i] = [
                section for section in mains.good_sections()
                if section.timedelta > min_duration
            ]

    def _gen_single_example(self, validation=False):
        buildings = (self.validation_buildings if validation
                     else self.train_buildings)
        building_i = self.rng.choice(buildings)
        elec = self.dataset.buildings[building_i].elec
        section = self.rng.choice(self.good_sections[building_i])
        section_duration = section.timedelta.total_seconds()
        max_duration = self.sample_period * self.seq_length
        latest_start = section_duration - max_duration
        relative_start = self.rng.randint(0, latest_start)
        start = section.start + timedelta(seconds=relative_start)
        end = start + timedelta(seconds=max_duration)
        sections = [TimeFrame(start, end)]
        mains_power = elec.mains().power_series(
            sample_period=self.sample_period, sections=sections).next()
        appliances_power = self.metergroups[building_i].dataframe_of_meters(
            sample_period=self.sample_period, sections=sections)

        def truncate(data):
            n = len(data)
            assert n >= self.seq_length
            if n > self.seq_length:
                data = data[:self.seq_length]
            return data
        mains_power = truncate(mains_power)
        appliances_power = truncate(appliances_power)
        appliances_power.columns = elec.get_labels(appliances_power.columns)

        # time of day
        index = mains_power.index.tz_localize(None)
        secs_into_day = (index.astype(int) / 1E9) % SECS_PER_DAY
        time_of_day = ((secs_into_day / SECS_PER_DAY) * 2.) - 1.


        return appliances_power, mains_power, time_of_day


def timestamp_to_int(ts):
    ts = pd.Timestamp(ts)
    return ts.asm8.astype('datetime64[s]').astype(int)


class NILMTKSourceOld(Source):
    def __init__(self, filename, appliances, building=1):
        """
        Parameters
        ----------
        filename : str
        appliances : list of strings
            The first one is the target appliance
        building : int
        """
        super(NILMTKSource, self).__init__(
            seq_length=14400,
            n_seq_per_batch=5,
            n_inputs=1000,
            n_outputs=1)
        self.sample_period = 6
        self.min_power = 20
        self.max_power = 200
        self.dataset = DataSet(filename)
        self.appliances = appliances
        self._tz = self.dataset.metadata['timezone']
        self.metergroup = self.dataset.buildings[building].elec

    def _get_data_for_single_day(self, start):
        start = pd.Timestamp(start).date()
        end = start + timedelta(days=1)
        timeframe = TimeFrame(start, end, tz=self._tz)
        load_kwargs = dict(sample_period=self.sample_period,
                           sections=[timeframe])

        # Load output (target) data
        app = self.metergroup[self.appliances[0]]
        y = app.power_series_all_data(**load_kwargs)
        if y is None or y.max() < self.min_power:
            return None, None

        # Load input (aggregate) data
        app = y + self.metergroup[self.appliances[1]]
        X = app.power_series_all_data(**load_kwargs)
        for appliance in self.appliances[2:]:
            app = self.metergroup[appliance]
            X += app.power_series_all_data(**load_kwargs)

        freq = "{:d}S".format(self.sample_period)
        index = pd.date_range(start, end, freq=freq, tz=self._tz)

        def preprocess(data):
            data = data.fillna(0)
            data = data.clip(upper=self.max_power)
            data[data < self.min_power] = 0
            data = data.reindex(index, fill_value=0)
            data /= self.max_power
            return data

        def index_as_minus_one_to_plus_one(data):
            index = data.index.astype(np.int64)
            index -= np.min(index)
            index = index.astype(np.float32)
            index /= np.max(index)
            return np.vstack([index, data.values]).transpose()

        X = preprocess(X).diff().dropna().values
        y = preprocess(y).diff().dropna().values
        return X, y

    def _gen_unquantized_data(self, validation=False):
        X = np.empty(shape=(self.n_seq_per_batch, self.seq_length, 1))
        y = np.empty(shape=self.output_shape())
        N_DAYS = 600  # there are more like 632 days in the dataset
        FIRST_DAY = pd.Timestamp("2013-04-12")
        seq_i = 0
        while seq_i < self.n_seq_per_batch:
            if validation:
                low = N_DAYS
                high = N_DAYS + self.n_seq_per_batch
            else:
                low = 0
                high = N_DAYS
            days = self.rng.randint(low=low, high=high)
            start = FIRST_DAY + timedelta(days=days)
            X_one_seq, y_one_seq = self._get_data_for_single_day(start)

            if y_one_seq is not None:
                try:
                    X[seq_i, :, :] = X_one_seq.reshape(self.seq_length, 1)
                    y[seq_i, :, :] = y_one_seq.reshape(self.seq_length, 1)
                except ValueError as e:
                    self.logger.info(e)
                    self.logger.info("Skipping {}".format(start))
                else:
                    seq_i += 1
            else:
                self.logger.info("Skipping {}".format(start))
        return X, y

    def _gen_data(self, *args, **kwargs):
        X = kwargs.pop('X', None)
        if X is None:
            X, y = self._gen_unquantized_data(*args, **kwargs)
        else:
            y = None
        X_quantized = np.empty(shape=self.input_shape())
        for i in range(self.n_seq_per_batch):
            X_quantized[i, :, 0] = X[i, :, 0]  # time of day
            X_quantized[i, :, 1:] = quantize(X[i, :, 1], self.n_inputs)

        return X_quantized, y


class RandomSegments(Source):
    def __init__(self, filename, target_appliance,
                 train_buildings, validation_buildings,
                 window=(None, None),
                 sample_period=6,
                 ignore_incomplete=False,
                 on_power_threshold=50,
                 **kwargs):
        self.filename = filename
        self.dataset = DataSet(filename)
        self.dataset.set_window(*window)
        self.window = self.dataset.store.window
        self.tz = self.dataset.metadata['timezone']
        self.target_appliance = target_appliance
        self.train_buildings = train_buildings
        self.validation_buildings = validation_buildings
        self.sample_period = sample_period
        self.ignore_incomplete = ignore_incomplete
        self.on_power_threshold = on_power_threshold
        #        super(RandomSegments, self).__init__(n_outputs=1, n_inputs=1, **kwargs)
        super(RandomSegments, self).__init__(n_outputs=1, **kwargs)

    def _init_data(self):
        """Overridden by sub-classes."""
        self.good_sections = {}
        seq_duration_secs = self.seq_length * self.sample_period
        for building_i in self.get_all_buildings():
            elec = self.dataset.buildings[building_i].elec
            try:
                target_meter, _ = get_meter_for_appliance(
                    elec, self.target_appliance)
            except KeyError:
                self.logger.info("Building {} has no {}".format(building_i, self.target_appliance))
                self._remove_building(building_i)
                continue
            mains_good_sections = elec.mains().good_sections()
            target_good_sections = target_meter.good_sections()
            good_sections = mains_good_sections.intersection(target_good_sections)
            good_sections = good_sections.remove_shorter_than(seq_duration_secs)

            if len(good_sections) > 0:
                self.good_sections[building_i] = good_sections
            else:
                self.logger.info("Building {} has no good sections in window.".format(building_i))
                self._remove_building(building_i)

    def _remove_building(self, building_i):
        def remove(lst):
            try:
                lst.remove(building_i)
            except ValueError:
                pass

        self.logger.info("Removing building {} from validation_buildings and train_buildings.".format(building_i))
        remove(self.validation_buildings)
        remove(self.train_buildings)

    def get_all_buildings(self):
        return list(set(self.train_buildings + self.validation_buildings))

    def _gen_single_example(self, validation=False, valid_ratio=0.2):
        # Select a building
        buildings = (self.validation_buildings if validation
                     else self.train_buildings)
        building_i = self.rng.choice(buildings, 1)[0]

        # Select a good section from building
        good_sections = self.good_sections[building_i]
        timeframe = self._select_section_and_time_and_load(
            validation, good_sections, valid_ratio=valid_ratio)
        mains = self._load_data('mains', building_i, timeframe)
        appliance = self._load_data('target', building_i, timeframe)
        return mains, appliance

    def _select_section_and_time_and_load(self, validation, good_sections, valid_ratio=0.2, building=None):
        if set(self.train_buildings) == set(self.validation_buildings):
            ###
            n_validation_activations = int(valid_ratio * len(good_sections))
            if n_validation_activations > self.n_seq_per_batch:
                n_validation_activations = 64
                ###
            if validation:
                good_sections = good_sections[:n_validation_activations]
            else:
                good_sections = good_sections[n_validation_activations:]
        if len(good_sections) > 0:  # 1:
            # good_section_i = self.rng.randint(low=0, high=len(good_sections)-1)
            good_section_i = self.rng.randint(low=0, high=len(good_sections))
            good_section = good_sections[good_section_i]  #
        else:
            # good_section_i = 0
            good_section = TimeFrame(start='2014-01-01 09:41:00-08:00', end='2014-01-01 10:39:00-08:00')

        # Select a time window from within good section
        if self.nus_enabled:
            # samples offset instead of temporal offset
            max_start_offset_samples = good_section.samples - self.seq_length if good_section.samples else good_section.samples
            if max_start_offset_samples > 0:
                offset_samples = self.rng.randint(0, max_start_offset_samples)
            else:
                offset_samples = 0

            # handle the use of the fake good_section
            _main = self.mains[building][good_section.start:good_section.end]
            if len(_main) - (offset_samples+self.seq_length) > 0:
                start = _main.index[offset_samples]
                end = _main.index[offset_samples+self.seq_length-1]
                timeframe = TimeFrame(start, end, samples=self.seq_length)
            else:
                # start = self.mains[building].index[0] + timedelta(seconds=self.sample_period)
                # end = self.mains[building].index[0] + timedelta(seconds=(self.seq_length + 1) * self.sample_period)
                # timeframe = TimeFrame(start=start, end=end)
                timeframe = TimeFrame(start=good_section.start, end=good_section.end)

        else:
            seq_duration_secs = self.seq_length * self.sample_period
            max_start_offset_secs = (good_section.timedelta.total_seconds() -
                                     seq_duration_secs)
            if max_start_offset_secs > 0:
                offset_secs = self.rng.randint(0, max_start_offset_secs)
            else:
                offset_secs = 0
            offset = timedelta(seconds=offset_secs)
            start = good_section.start + offset
            end = start + timedelta(seconds=seq_duration_secs)
            timeframe = TimeFrame(start, end)

        return timeframe

    def _load_data(self, mains_or_target, building_i, timeframe):
        elec = self.dataset.buildings[building_i].elec
        if mains_or_target == 'mains':
            electric = elec.mains()
        elif mains_or_target == 'target':
            electric = elec[self.target_appliance]
        else:
            raise RuntimeError("Unrecognised: '" + mains_or_target + "'")
        data = electric.power_series_all_data(
            sections=[timeframe], sample_period=self.sample_period)
        if mains_or_target == 'target' and self.ignore_incomplete:
            data = self._remove_incomplete(data)
        data.fillna(0, inplace=True)
        data = data.values[:self.seq_length]
        pad_width = self.seq_length - len(data)
        data = np.pad(data, (0, pad_width), 'constant')
        return data

    def _load_r_data(self, mains_or_target, building_i, timeframe):
        elec = self.dataset.buildings[building_i].elec
        if mains_or_target == 'mains':
            electric = elec.mains()
        elif mains_or_target == 'target':
            electric = elec[self.target_appliance]
        else:
            raise RuntimeError("Unrecognised: '" + mains_or_target + "'")
        data = electric.power_series_all_data(
            sections=[timeframe], sample_period=self.sample_period)
        if mains_or_target == 'target' and self.ignore_incomplete:
            data = self._remove_incomplete(data)
        data.fillna(0, inplace=True)
        data = data.values[:self.seq_length]
        pad_width = self.seq_length - len(data)
        data = np.pad(data, (0, pad_width), 'constant')
        return data

    def _gen_data(self, validation=False, valid_ratio=0.2):
        X = np.zeros(self.input_shape(), dtype=np.float32)
        y = np.zeros(self.output_shape(), dtype=np.float32)
        N_RETRIES = 10
        for i in range(self.n_seq_per_batch):
            for retry in range(N_RETRIES):
                single_X, single_y = self._gen_single_example(validation, valid_ratio=valid_ratio)
                if single_X is not None and single_y is not None:
                    single_X = single_X.reshape((self.input_shape()[1], self.input_shape()[2]))
                    X[i, :, :], y[i, :, 0] = single_X, single_y
                    break
        return X, y

    def get_labels(self):
        return [self.target_appliance]

    def _remove_incomplete(self, data):
        activations = activation_series_for_chunk(
            data, on_power_threshold=self.on_power_threshold)
        new_data = pd.Series(0, index=data.index)
        for activation in activations:
            if activation is not None:
                new_data = new_data.add(activation, fill_value=0)
        return new_data


class RandomSegmentsInMemory(RandomSegments):
    def _init_data(self):
        super(RandomSegmentsInMemory, self)._init_data()
        self.data = OrderedDict()
        for building_i in self.get_all_buildings():
            elec = self.dataset.buildings[building_i].elec
            try:
                meter, target_app = get_meter_for_appliance(
                    elec, self.target_appliance)
            except KeyError:
                self.logger.info(
                    "Building {} has no {}"
                        .format(building_i, self.target_appliance))
                self._remove_building(building_i)
                continue
            target = meter.power_series_all_data(
                sample_period=self.sample_period)
            if target is None:
                self.logger.info(
                    "Building {} has no target data in time window."
                        .format(building_i))
                self._remove_building(building_i)
            else:
                target.fillna(0, inplace=True)
                self.data[building_i] = {
                    'mains': elec.mains().power_series_all_data(
                        sample_period=self.sample_period).dropna(),
                    'target': target
                }
            gc.collect()

    def _load_data(self, mains_or_target, building_i, timeframe):
        try:
            building_data = self.data[building_i]
        except KeyError:
            return
        data = building_data[mains_or_target][timeframe.start:timeframe.end]
        if mains_or_target == 'target' and self.ignore_incomplete:
            data = self._remove_incomplete(data)
        data = data.values[:self.seq_length]
        pad_width = self.seq_length - len(data)
        data = np.pad(data, (0, pad_width), 'constant')
        return data


class SameLocation(RandomSegments):
    def __init__(self, appliances,
                 offset_probability=0,
                 ignore_offset_activations=False,
                 clip_appliance_power=False,
                 max_appliance_power=1000,
                 skip_probability=0,
                 allow_incomplete=False,
                 ignore_incomplete=False,
                 include_all=False,
                 divide_target_by=1,
                 window_per_building=None,
                 window=(None, None),
                 load_mains=True,
                 main_mode='denoised',
                 on_power_threshold=40,
                 min_on_duration=12,
                 min_off_duration=12,
                 *args,
                 **kwargs):
        self.appliances = appliances
        self.offset_probability = offset_probability
        self.ignore_offset_activations = ignore_offset_activations
        self.clip_appliance_power = clip_appliance_power
        self.max_appliance_power = max_appliance_power
        self.skip_probability = skip_probability
        self.allow_incomplete = allow_incomplete
        self.include_all = include_all
        self.divide_target_by = divide_target_by
        self.load_mains = load_mains
        self.main_mode = main_mode
        self.min_off_duration = min_off_duration
        self.min_on_duration = min_on_duration
        if window_per_building and window != (None, None):
            raise ValueError(
                "Cannot set both `window_per_building` and `window`")
        self.window_per_building = window_per_building
        kwargs['ignore_incomplete'] = ignore_incomplete
        kwargs['window'] = window
        kwargs['on_power_threshold'] = on_power_threshold
        super(SameLocation, self).__init__(*args, **kwargs)

    def _init_data(self):
        """
        * Load all activations for target with nice sized border
        * Load all mains data into memory
        """
        self._load_activations()
        if self.load_mains:
            if self.main_mode == 'noised':
                self._load_mains()
                if self.n_inputs == 2:
                    if self.dataset.buildings[1].metadata['dataset'] == 'UK-DALE':
                        self.logger.info("UK Dale dataset detected, calculating reactive power.")
                        self._load_aprt_to_r_mains()
                    else:
                        self._load_r_mains()
            elif self.main_mode == 'denoised':
                self._main_sum_appliance()
                if self.n_inputs == 2:
                    self._load_r_main_sum_appliance()
        if self.skip_probability and self.load_mains:
            self._load_sections_without_target()
        self.dataset.store.close()

    def _load_sections_without_target(self):
        self.sections_without_target = {}
        for building_i in self.get_all_buildings():
            self.logger.info("Loading sections_without_target for building {:d}...".format(building_i))
            sections_without_target = TimeFrameGroup()
            mains_start = self.mains[building_i].index[0]
            prev_end = mains_start
            for activation in self.activations[building_i]:
                activation_start = activation.index[0]
                if prev_end >= activation_start:
                    continue
                try:
                    timeframe = TimeFrame(prev_end, activation_start)
                except ValueError as exception:
                    self.logger.info("Failed to create timeframe: building_i={}, mains_start={}, prev_end={},"
                                     " activation_start={}.  Exception={}".format(building_i, mains_start, prev_end,
                                                                                  activation_start, exception))
                else:
                    sections_without_target.append(timeframe)
                prev_end = activation.index[-1]
            mains_end = self.mains[building_i].index[-1]
            if prev_end < mains_end:
                sections_without_target.append(TimeFrame(prev_end, mains_end))
            sections_without_target = sections_without_target.intersection(self.target_good_sections[building_i])
            # sections_without_target = sections_without_target.remove_shorter_than(self.seq_length * self.sample_period)
            # self.logger.info("Loaded {:d} sections_without_target from house {:d}.".format(len(sections_without_target),
            #                                                                                building_i))
            # if len(sections_without_target) > 0:
            #     self.sections_without_target[building_i] = (sections_without_target)

            # compute TimeFrame.samples for each sections_without_target. It is done now to avoid modification of the intersection method
            if self.nus_enabled:
                for section in sections_without_target:
                    section.samples = self.nus_labels[building_i][section.start:section.end].sum()

                sections_without_target = sections_without_target.remove_shorter_than(samples=self.seq_length)

            else:
                sections_without_target = sections_without_target.remove_shorter_than(self.seq_length * self.sample_period)

            self.logger.info("Loaded {:d} sections_without_target from house {:d}.".format(len(sections_without_target),
                                                                                           building_i))

            if len(sections_without_target) > 0:
                self.sections_without_target[building_i] = (sections_without_target)

    def _gen_single_example(self, validation=False, valid_ratio=0.2):
        N_RETRIES = 256
        for retry in range(N_RETRIES):
            X, y = self._gen_single_example_(validation, valid_ratio=valid_ratio)
            """
            try:
                print(X.shape)   ##       
                print(y.shape)  ##
            except:
                print("no matrix")
            """
            if X is None:
                continue
            # if len(X) == self.seq_length:
            if X.shape[0] == self.seq_length:
                y /= self.divide_target_by
                return X, y
        if X is None:
            raise RuntimeError("X is None")
        else:
            raise RuntimeError("X has wrong length: {}".format(len(X)))

    def _gen_single_example_(self, validation=False, valid_ratio=0.2):
        """
        * pick building at random
        * pick an activation at random
        * data is that activation, padded at the end, plus mains at same time.
        """

        if self.nus_enabled and self.dataset.metadata['name'].lower().replace('-', '') == 'ampds' and self.target_appliance == 'fridge':
            N_LEAD_IN = 25
        else:
            N_LEAD_IN = 50  # number of samples to lead in with

        if self.us_enabled:
            N_LEAD_IN = N_LEAD_IN // self.us_subsampling_ratio

        # pick a building
        buildings = (self.validation_buildings if validation
                     else self.train_buildings)
        building_i = self.rng.choice(buildings, 1)[0]

        # pick an activation
        activations = self.activations[building_i]
        if set(self.validation_buildings) == set(self.train_buildings):

            n_validation_activations = int(valid_ratio * len(activations))  # 20% of total activations (real) used in validation
            if n_validation_activations > self.n_seq_per_batch:
                n_validation_activations = 64
            elif n_validation_activations == 0:
                n_validation_activations = 1

            if validation:
                activations = activations[:n_validation_activations]
            else:
                activations = activations[n_validation_activations:]

        if len(activations) == 0:
            raise RuntimeError("No activations.  validation={}".format(validation))

        if len(activations) == 1:
            activation_i = 0  # randint retunrs error if low == high
        else:
            activation_i = self.rng.randint(low=0, high=len(activations) - 1)

        activation = activations[activation_i]

        if self.rng.binomial(n=1, p=self.skip_probability):
            return self._seq_without_target(building_i, validation)

        y = np.pad(activation.values, (N_LEAD_IN, 0), 'constant')

        def zeros():
            return np.zeros(self.seq_length, dtype=np.float32)

        # random offset (number of samples)
        if self.rng.binomial(n=1, p=self.offset_probability):
            if self.rng.binomial(n=1, p=0.5):
                # shift backwards
                if self.allow_incomplete:
                    max_offset = N_LEAD_IN + len(activation)
                else:
                    max_offset = N_LEAD_IN
                offset = self.rng.randint(low=1, high=max_offset)
                if self.ignore_incomplete and offset >= N_LEAD_IN:
                    y = zeros()
                else:
                    y = y[offset:]
                offset_direction = 'backwards'
            else:
                # shift forwards
                if self.allow_incomplete:
                    max_offset = self.seq_length - N_LEAD_IN
                else:
                    max_offset = self.seq_length - N_LEAD_IN - len(activation)
                if max_offset > 1:
                    offset = self.rng.randint(low=1, high=max_offset)
                else:
                    offset = 0
                if self.ignore_incomplete and offset >= (
                        self.seq_length - N_LEAD_IN - len(activation)):
                    y = zeros()
                else:
                    y = np.pad(y, (offset, 0), 'constant')
                offset_direction = 'forwards'
            if self.ignore_offset_activations:
                y = zeros()
        else:
            offset = 0

        # clip or pad to get seq length to be exactly self.seq_length samples
        def clip_or_pad(data):
            data = data[:self.seq_length]
            n_zeros_to_pad = self.seq_length - len(data)
            data = np.pad(data, (0, n_zeros_to_pad), 'constant')
            return data

        if self.ignore_incomplete and len(y) > self.seq_length:
            y = zeros()
        else:
            y = clip_or_pad(y)

        # get mains
        mains = self.mains[building_i]
        if self.n_inputs == 2:
            r_mains = self.r_mains[building_i]

        if self.nus_enabled:
            start_samples_loc = mains.index.get_loc(activation.index[0]) - N_LEAD_IN

            if offset:
                if offset_direction == 'backwards':
                    start_samples_loc += offset
                else:  # 'forward'
                    start_samples_loc -= offset

            X = mains[start_samples_loc:start_samples_loc+self.seq_length]

        else:
            start = activation.index[0] - timedelta(
                seconds=N_LEAD_IN * self.sample_period)
            if offset:
                offset_timedelta = timedelta(seconds=offset * self.sample_period)
                if offset_direction == 'backwards':
                    start += offset_timedelta
                else:
                    start -= offset_timedelta

            end = start + timedelta(seconds=(self.seq_length * self.sample_period) - 1)
            X = mains[start:end]

            if self.n_inputs == 2:
                rX = r_mains[start:end]

        if len(X) != self.seq_length:
            return None, None

        # Check previous activation isn't in mains
        if activation_i > 0:
            if activations[activation_i - 1].index[-1] > X.index[0]:
                return None, None

        if self.include_all:
            y_series = pd.Series(y, index=X.index)
            # check activations backwards
            for i in range(activation_i - 1, -1, -1):
                other_activation = activations[i]
                if (other_activation.index[-1] > start and
                        (not self.ignore_incomplete or
                         other_activation.index[0] > start)):
                    y_series = y_series.add(
                        other_activation[start:end], fill_value=0)
                else:
                    break

            # check activations forwards
            for i in range(activation_i + 1, len(activations)):
                other_activation = activations[i]
                if (other_activation.index[0] < end and
                        (not self.ignore_incomplete or
                         other_activation.index[-1] < end)):
                    y_series = y_series.add(
                        other_activation[start:end], fill_value=0)
                else:
                    break

            y = y_series.values

        if self.n_inputs == 2:
            X = np.array([X.values, rX.values]).T
        else:
            X = X.values

        return X, y

    def _seq_without_target(self, building_i, validation):
        try:
            sections_without_target = self.sections_without_target[building_i]
        except KeyError:
            return None, None

        timeframe = self._select_section_and_time_and_load(
            validation, sections_without_target, building=building_i)

        y = np.zeros(self.seq_length, dtype=np.float32)
        X = self.mains[building_i][timeframe.start:timeframe.end]

        if self.n_inputs == 2:
            rX = self.r_mains[building_i][timeframe.start:timeframe.end]
            X = np.array([X.values, rX.values])
            X = X.T
        else:
            X = X.values

        return X, y

    def _load_activations(self):
        activations = OrderedDict()

        if self.nus_enabled:
            nus_labels = OrderedDict()
            self.logger.info("Subsampling activations to apply NUS")

        self.target_good_sections = {}
        for building_i in self.get_all_buildings():

            if __debug__:
                original_activations_length = []
                nus_activations_length = []

            if self.window_per_building:
                window = self.window_per_building[building_i]
                self.logger.info("Setting window for building {} to (start={}, end={})".format(building_i, *window))
                self.dataset.set_window(*window)
            elec = self.dataset.buildings[building_i].elec
            try:
                meter, target_app = get_meter_for_appliance(
                    elec, self.target_appliance)
            except KeyError:
                self.logger.info("Building {} has no {}".format(building_i, self.target_appliance))
                self._remove_building(building_i)
                continue
            self.target_good_sections[building_i] = meter.good_sections()
            if self.nus_enabled:
                # activation must include enough samples around the state-change for the expansion
                activation_series = meter.activation_series(
                    on_power_threshold=self.on_power_threshold,
                    min_off_duration=self.min_off_duration,
                    min_on_duration=self.min_on_duration,
                    border=self.nus_expansion_ratio // 2)
            elif self.us_enabled:
                # activation must include enough samples around the state-change to get at least one samples before and after the change state in the uniform subsampling
                activation_series = meter.activation_series(
                    on_power_threshold=self.on_power_threshold,
                    min_off_duration=self.min_off_duration,
                    min_on_duration=self.min_on_duration,
                    border=self.us_subsampling_ratio)
            else:
                activation_series = meter.activation_series(
                    on_power_threshold=self.on_power_threshold,
                    min_off_duration=self.min_off_duration,
                    min_on_duration=self.min_on_duration)

            activations[building_i] = _preprocess_activations(
                activation_series,
                sample_period=self.sample_period,
                max_power=self.max_appliance_power,
                clip_appliance_power=self.clip_appliance_power)
            self.logger.info("Loaded {:d} {:s} activations from house {:d}.".format(len(activation_series),
                                                                                    target_app,
                                                                                    building_i))
            if len(activation_series) == 0:
                del activations[building_i]
                self._remove_building(building_i)
            gc.collect()

            if self.nus_enabled:
                t0 = time.time()
                # create general subsampled timeseries for given window

                # create 0s-sereis at original freq
                subsampled_labels = pd.Series(0, index=pd.date_range(start=window[0], end=window[1], freq='{}S'.format(self.sample_period),tz=self.tz))

                # set to 1 the samples at subsample ratio
                subsampled_ratio_indexes = pd.date_range(start=window[0], end=window[1], freq='{}S'.format(self.sample_period * self.nus_subsampling_ratio),
                                                         tz=self.dataset.metadata.get('timezone'))
                subsampled_labels[subsampled_ratio_indexes] = 1

                # load ground truth for each appliance and specific building
                tmp_dataset_name = self.dataset.metadata['name'].lower().replace('-', '')

                for appl in self.appliances:
                    if isinstance(appl, list):
                        if appl[0] in ['fridge freezer', 'fridge', 'freezer']:
                            appl = 'fridge'
                        elif appl[0] in ['washer dryer', 'washing machine']:
                            appl = 'washing machine'
                        else:
                            raise TypeError('Unrecognize appliance.')

                    # fix missing appliance for building 2
                    # todo:  in self.appliances add list for each building
                    if tmp_dataset_name == 'redd' and building_i == 2 and appl == 'washer dryer':
                        continue

                    if __debug__:
                        csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.splitext(os.path.join(self.filename))[0], building_i, appl.replace(' ', '_'))
                    else:
                        # tmp_dataset_name = self.dataset.metadata['name'].lower().replace('-', '')
                        tmp_csv_dir = os.path.dirname(os.path.dirname(self.filename))
                        csv_filename = "{}-ground_truth-{}-{}.csv".format(os.path.join(tmp_csv_dir, tmp_dataset_name), building_i,  appl.replace(' ', '_'))

                    # this couse ambigous error for dates nar dst
                    # appliance_ground_truth = pd.Series.from_csv(csv_filename)
                    # appliance_ground_truth = appliance_ground_truth.tz_localize('UTC').tz_convert(tz=self.dataset.metadata.get('timezone'))  # fix timezone
                    # drop index outside actualwindow
                    # appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < window[0]], inplace=True)
                    # appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > window[1]], inplace=True)

                    # avoid ambigous error for date near dst
                    appliance_ground_truth = pd.Series.from_csv(csv_filename).tz_localize('UTC')
                    w0 = pd.Timestamp(window[0]).tz_localize(tz=self.tz).tz_convert('UTC')
                    w1 = pd.Timestamp(window[1]).tz_localize(tz=self.tz).tz_convert('UTC')
                    appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index < w0], inplace=True)
                    appliance_ground_truth.drop(labels=appliance_ground_truth.index[appliance_ground_truth.index > w1], inplace=True)
                    appliance_ground_truth = appliance_ground_truth.tz_convert(tz=self.tz)

                    for each in range(-self.nus_expansion_ratio//2,self.nus_expansion_ratio//2):
                        expansion_indices = appliance_ground_truth.index + timedelta(seconds=each * self.sample_period)
                        # check if expansion indices in subsampled_labels range, remove outliers
                        expansion_indices = expansion_indices[(expansion_indices >= subsampled_labels.index[0]) & (expansion_indices <= subsampled_labels.index[-1])]
                        subsampled_labels[expansion_indices] = 1

                self.logger.info("Created NUS labels vector for building " + str(building_i))
                nus_labels[building_i] = subsampled_labels

                # filter activation
                self.logger.info("Start filtering activations...")
                any_empty_activation = False
                # pre-clean labels
                labels = subsampled_labels[subsampled_labels == 0].index

                for index, activation in enumerate(activations[building_i]):
                    # print("Activation original size: {}".format(len(activation)))

                    if __debug__:
                        original_activations_length.append(len(activation))

                    try:
                        # labels to drop
                        # labels = subsampled_labels[subsampled_labels == 0].index
                        _labels = labels[(labels >= activation.index[0]) & (labels <= activation.index[-1])]

                        # alternative: to improve performace
                        # subsampled_labels[(subsampled_labels >= activation.index[0]) & (subsampled_labels <= activation.index[-1])]

                        # print("Activatoin length: {}".format(len(activation)))
                        if not _labels.date.any():
                            # do avoid strage errors, drop labels only if exist
                            # print("Activation length after subsampling: {}".format(len(activation)))
                            continue

                        # activation.drop(labels=labels, inplace=True)
                        # print("Activation length after subsampling: {}".format(len(activation)))
                        activations[building_i][index] = activation.tz_convert('UTC').drop(labels=_labels.tz_convert('UTC')).tz_convert(self.tz)

                        if not any_empty_activation and not activations[building_i][index].any():
                            self.logger.warning("Activation series subsampled to empty series.")
                            any_empty_activation = True

                        # statistical info
                        if __debug__:
                            if activation.any():
                                nus_activations_length.append(len(activations[building_i][index]))

                    except:
                        self.logger.exception("NUS - unhandled error in activations subsampling:\n")
                        raise Exception

                # avoid to process series if no empty activations
                if any_empty_activation:
                    activations[building_i] = [activation for activation in activations[building_i] if activation.any()]
                    self.logger.info("{:s} activations reduced to {:d} for house {:d}.".format(target_app, len(activations[building_i]), building_i))

                self.logger.debug("NUS activation exe time: {}".format(time.time()-t0))

            if __debug__:
                # count the number of activations with a length <= seq_length
                valid_activations = 0
                for index, activation in enumerate(activations[building_i]):
                    if activations[building_i][index].size <= self.seq_length:
                        valid_activations += 1
                self.logger.info("Valid activations for building {}: {}/{}".format(building_i, valid_activations, len(activations[building_i])))

                # statistical info
                original_activations_length = np.asarray(original_activations_length)
                nus_activations_length = np.asarray(nus_activations_length)
                self.logger.debug("Original number of activations: {}".format(len(original_activations_length)))
                self.logger.debug("NUS number of activations: {}".format(len(nus_activations_length)))
                self.logger.debug("Original activations avg length:  {}".format(np.mean(original_activations_length)))
                self.logger.debug("NUS activations avg length: {}".format(np.mean(nus_activations_length)))
                self.logger.debug("Original activations std length: {}".format(np.std(original_activations_length)))
                self.logger.debug("NUS activations std length: {}".format(np.std(nus_activations_length)))

        self.activations = activations

        if self.nus_enabled:
            self.nus_labels = nus_labels

    def _load_mains(self):
        mains = OrderedDict()
        for building_i in self.get_all_buildings():
            self.logger.info(
                "Loading mains data for building {:d}...".format(building_i))
            if self.window_per_building:
                window = self.window_per_building[building_i]
                self.logger.info("Setting window for building {} to (start={}, end={})".format(building_i, *window))
                self.dataset.set_window(*window)
            elec = self.dataset.buildings[building_i].elec
            meter = elec.mains()
            mains_data = meter.power_series_all_data(
                sample_period=self.sample_period)
            mains_data = mains_data.dropna()
            mains[building_i] = mains_data
            self.logger.info("Loaded mains data for building {:d}.".format(building_i))
            gc.collect()

            # Check if any activations start *before* mains starts
            remove_activations_before_index = 0
            for i, activation in enumerate(self.activations[building_i]):
                if activation.index[0] < mains_data.index[0]:
                    remove_activations_before_index = i + 1
            if remove_activations_before_index > 0:
                self.logger.info("Mains starts after activation {} so will remove previous activations.".format(
                    remove_activations_before_index))
                self.activations[building_i] = self.activations[building_i][remove_activations_before_index:]

            # apply NUS to mains
            if self.nus_enabled:

                t0 = time.time()

                try:

                    labels = self.nus_labels[building_i][self.nus_labels[building_i] == 0].index
                    labels = labels[(labels >= mains[building_i].index[0]) & (labels <= mains[building_i].index[-1])]

                    # to avoid dts error
                    _mains = mains[building_i].tz_convert('UTC')
                    _mains.drop(labels=labels.tz_convert('UTC'), inplace=True)
                    mains[building_i] = _mains.tz_convert(self.tz)

                except:
                    self.logger.exception("Unhandled error in NUS mains generation:\n")

                self.logger.debug("NUS mains exe time: {}".format(time.time() - t0))

        self.mains = mains

    def _main_sum_appliance(self):
        mains = OrderedDict()
        if self.nus_enabled:
            self.logger.info("Subsampling mains to apply NUS")

        for building_i in self.get_all_buildings():
            self.logger.info(
                "Loading mains data for building {:d}...".format(building_i))
            if self.window_per_building:
                window = self.window_per_building[building_i]
                self.logger.info("Setting window for building {} to (start={}, end={})".format(building_i, *window))
                self.dataset.set_window(*window)
            elec = self.dataset.buildings[building_i].elec
            meter = elec.mains()
            mains_data = meter.power_series_all_data(sample_period=self.sample_period)
            # mains_data = mains_data.dropna()
            mains_data -= mains_data

            meters = get_meters_for_appliances(
                elec, self.appliances, self.logger)

            for appliance_i, meter in enumerate(meters):
                main_app = meter.power_series_all_data(sample_period=self.sample_period)
                # main_app=main_app.dropna()
                mains_data += main_app
            mains_data = mains_data.dropna()
            mains[building_i] = mains_data
            self.logger.info("Loaded mains data for building {:d}.".format(building_i))
            gc.collect()

            # Check if any activations start *before* mains starts and end *after* mains ends
            remove_activations_before_index = 0
            remove_activations_after_index = len(self.activations[building_i])
            try:
                for i, activation in enumerate(self.activations[building_i]):
                    if activation.index[0] < mains_data.index[0]:
                        remove_activations_before_index = i + 1
                    if activation.index[-1] > mains_data.index[-1] and remove_activations_after_index == len(self.activations[building_i]):
                        # stop at the first index
                        remove_activations_after_index = i
                if remove_activations_before_index > 0 or remove_activations_after_index < len(self.activations[building_i]):
                    self.logger.info("Mains starts after activation {} and ends before activation {} so will remove previous/forward activations."
                                     .format(remove_activations_before_index, remove_activations_after_index))
                    self.activations[building_i] = self.activations[building_i][remove_activations_before_index:remove_activations_after_index]

            except:
                self.logger.critical("Unhandled error in _main_sum_appliance:\n")

            # apply NUS to mains
            if self.nus_enabled:
                t0 = time.time()

                try:

                    labels = self.nus_labels[building_i][self.nus_labels[building_i] == 0].index
                    labels = labels[(labels >= mains[building_i].index[0]) & (labels <= mains[building_i].index[-1])]

                    # to avoid dts error
                    _mains = mains[building_i].tz_convert('UTC')
                    _mains.drop(labels=labels.tz_convert('UTC'), inplace=True)
                    mains[building_i] = _mains.tz_convert(self.tz)

                except:
                    self.logger.exception("Unhandled error in NUS mains generation:\n")

                self.logger.debug("NUS mains exe time: {}".format(time.time() - t0))

        self.mains = mains

    def _load_r_mains(self):
        mains = OrderedDict()
        for building_i in self.get_all_buildings():
            self.logger.info(
                "Loading mains data for building {:d}...".format(building_i))
            if self.window_per_building:
                window = self.window_per_building[building_i]
                self.logger.info("Setting window for building {} to (start={}, end={})".format(building_i, *window))
                self.dataset.set_window(*window)
            elec = self.dataset.buildings[building_i].elec
            meter = elec.mains()
            # kwargs['physical_quantity'] = 'power'
            # kwargs.setdefault('ac_type', 'best')
            mains_data = meter.power_series_all_data(sample_period=self.sample_period,
                                                     physical_quantity='power',
                                                     ac_type='reactive')
            mains_data = mains_data.dropna()
            mains[building_i] = mains_data
            self.logger.info("Loaded mains data for building {:d}.".format(building_i))
            gc.collect()

            # Check if any activations start *before* mains starts
            remove_activations_before_index = 0
            for i, activation in enumerate(self.activations[building_i]):
                if activation.index[0] < mains_data.index[0]:
                    remove_activations_before_index = i + 1
            if remove_activations_before_index > 0:
                self.logger.info("Mains starts after activation {} so will remove previous activations.".format(
                    remove_activations_before_index))
                self.activations[building_i] = self.activations[building_i][remove_activations_before_index:]

        self.r_mains = mains

    # ___________________________
    def _load_aprt_to_r_mains(self):
        mains = OrderedDict()
        for building_i in self.get_all_buildings():
            self.logger.info("Loading mains data for building {:d}...".format(building_i))
            if self.window_per_building:
                window = self.window_per_building[building_i]
                self.logger.info("Setting window for building {} to (start={}, end={})".format(building_i, *window))
                self.dataset.set_window(*window)
            elec = self.dataset.buildings[building_i].elec
            meter = elec.mains()
            # kwargs['physical_quantity'] = 'power'
            # kwargs.setdefault('ac_type', 'best')

            mains_data_active = meter.power_series_all_data(
                sample_period=self.sample_period, physical_quantity='power', ac_type='active')

            # In questo caso, usando l'UK DALE, devo prelevare l'apparente e poi ricavare una variabile con stessa
            # struttura ma che contiene la reattiva
            mains_data_aprt = meter.power_series_all_data(
                sample_period=self.sample_period, physical_quantity='power', ac_type='apparent')

            quad_aprt = np.square(mains_data_aprt.values)
            quad_acti = np.square(mains_data_active.values)
            react_array = np.sqrt(quad_aprt - quad_acti)

            mains_data = meter.power_series_all_data(
                sample_period=self.sample_period)

            for w in range(0, len(mains_data.values)):
                mains_data.values[w] = react_array[w]

            mains_data = mains_data.dropna()
            mains[building_i] = mains_data
            self.logger.info("Loaded mains data for building {:d}.".format(building_i))
            gc.collect()

            # Check if any activations start *before* mains starts
            remove_activations_before_index = 0
            for i, activation in enumerate(self.activations[building_i]):
                if activation.index[0] < mains_data.index[0]:
                    remove_activations_before_index = i + 1
            if remove_activations_before_index > 0:
                self.logger.info("Mains starts after activation {} so will remove previous activations.".format(
                    remove_activations_before_index))
                self.activations[building_i] = self.activations[building_i][remove_activations_before_index:]

        self.r_mains = mains

    def _load_r_main_sum_appliance(self):
        mains = OrderedDict()
        for building_i in self.get_all_buildings():
            self.logger.info("Loading mains data for building {:d}...".format(building_i))
            if self.window_per_building:
                window = self.window_per_building[building_i]
                self.logger.info("Setting window for building {} to (start={}, end={})".format(building_i, *window))
                self.dataset.set_window(*window)
            elec = self.dataset.buildings[building_i].elec

            meter = elec.mains()
            mains_data = meter.power_series_all_data(
                sample_period=self.sample_period)
            # mains_data = mains_data.dropna()
            mains_data -= mains_data

            meters = get_meters_for_appliances(
                elec, self.appliances, self.logger)

            for appliance_i, meter in enumerate(meters):
                main_app = meter.power_series_all_data(
                    sample_period=self.sample_period, physical_quantity='power', ac_type='reactive')
                # main_app=main_app.dropna()
                mains_data += main_app
            mains_data = mains_data.dropna()
            mains[building_i] = mains_data
            self.logger.info("Loaded mains data for building {:d}.".format(building_i))
            gc.collect()

            # Check if any activations start *before* mains starts
            remove_activations_before_index = 0
            for i, activation in enumerate(self.activations[building_i]):
                if activation.index[0] < mains_data.index[0]:
                    remove_activations_before_index = i + 1
            if remove_activations_before_index > 0:
                self.logger.info("Mains starts after activation {} so will remove previous activations.".format(
                    remove_activations_before_index))
                self.activations[building_i] = self.activations[building_i][
                                               remove_activations_before_index:]

        self.r_mains = mains


class MultiSource(Source):
    def __init__(self, sources, standardisation_source, **kwargs):
        """
        Parameters
        ----------
        sources : list of dicts
            keys are:
            - source : Source
            - train_probability : float [0, 1]
            - validation_probability : float [0, 1]
            standardisation_source : Source
            The source to take standardisation stats from
        """
        for source_dict in sources:
            source = source_dict['source']
            source.input_stats = standardisation_source.input_stats
            source.target_stats = standardisation_source.target_stats
        self.sources = sources
        self.standardisation_source = standardisation_source
        super(MultiSource, self).__init__(  # ------
            seq_length=standardisation_source.seq_length,
            n_seq_per_batch=standardisation_source.n_seq_per_batch,
            n_inputs=standardisation_source.n_inputs,
            n_outputs=standardisation_source.n_outputs,
            **kwargs
        )

    def get_batch(self, validation=False, valid_ratio=0.2, standardize=True):
        key = 'validation_probability' if validation else 'train_probability'
        probabilities = [source_dict[key] for source_dict in self.sources]
        source_i = self.rng.choice(len(self.sources), p=probabilities)
        source = self.sources[source_i]['source']
        batch = source.get_batch(validation, valid_ratio=valid_ratio, standardize=standardize)
        batch.metadata['source_i'] = source_i
        return batch

    def get_labels(self):
        return self.standardisation_source.get_labels()


def quantize(data, n_bins, all_hot=True, range=(-1, 1), length=None):
    midpoint = n_bins // 2
    if length is None:
        length = len(data)
    out = np.empty(shape=(length, n_bins))
    for i in np.arange(length):
        d = data[i]
        hist, _ = np.histogram(d, bins=n_bins, range=range)
        if all_hot:
            where = np.where(hist == 1)[0][0]
            if where > midpoint:
                hist[midpoint:where] = 1
            elif where < midpoint:
                hist[where:midpoint] = 1
        out[i, :] = hist
    return (out * 2) - 1


def standardise(X, how='range=2', mean=None, std=None, midrange=None, ptp=None):
    if how == 'std=1':
        if mean is None:
            mean = X.mean()
        if std is None:
            std = X.std()
        centered = X - mean
        if std == 0:
            return centered
        else:
            return centered / std
    elif how == 'range=2':
        if midrange is None:
            midrange = (X.max() + X.min()) / 2
        if ptp is None:
            ptp = X.ptp()
        return (X - midrange) / (ptp / 2)
    else:
        raise RuntimeError("unrecognised how '" + how + "'")


def discretize_scalar(X, n_bins=10, all_hot=False, boolean=True):
    output = np.zeros(n_bins)
    bin_i = int(X * n_bins)
    bin_i = min(bin_i, n_bins - 1)
    output[bin_i] = 1 if boolean else ((X * n_bins) - bin_i)
    if all_hot:
        output[:bin_i] = 1
    return output


def discretize(X, n_bins=10, **kwargs):
    assert X.shape[2] == 1
    output = np.zeros((X.shape[0], X.shape[1], n_bins))
    for i in range(X.shape[0]):
        for j in range(X.shape[1]):
            for k in range(X.shape[2]):
                output[i, j, :] = discretize_scalar(
                    X[i, j, k], n_bins, **kwargs)
    return output


def fdiff(X):
    assert X.shape[2] == 1
    output = np.zeros(X.shape)
    for i in range(X.shape[0]):
        output[i, :-1, 0] = np.diff(X[i, :, 0])
    return output


def power_and_fdiff(X):
    assert X.shape[2] == 1
    output = np.zeros((X.shape[0], X.shape[1], 2))
    for i in range(X.shape[0]):
        output[i, :, 0] = X[i, :, 0]
        output[i, :-1, 1] = np.diff(X[i, :, 0])
    return output


def get_meters_for_appliances(elec, appliances, logger):
    meters = []
    for appliance_i, apps in enumerate(appliances):
        if not isinstance(apps, list):
            apps = [apps]
        for appliance in apps:
            try:
                meter = elec[appliance]
            except KeyError:
                pass
            else:
                meters.append(meter)
                break
        else:
            logger.info(
                "  No appliance matching {} in building {}."
                    .format(apps, elec.building()))
            continue
    return meters


def get_meter_for_appliance(elec, target_appliance):
    """
    Parameters
    ----------
    elec : nilmtk.MeterGroup
    target_appliance : list of strings or string

    Returns
    -------
    meter, target_app
    meter : nilmtk.ElecMeter or MeterGroup
    target_app : string
    """
    if isinstance(target_appliance, list):
        for app in target_appliance:
            try:
                meter = elec[app]
            except KeyError:
                continue
            else:
                target_app = app
                break
        else:
            raise KeyError()
    else:
        meter = elec[target_appliance]
        target_app = target_appliance

    return meter, target_app


def unstandardise(data, std, mean, maximum=None):
    unstandardised_data = (data * std) + mean
    if maximum is not None:
        unstandardised_data *= maximum
    return unstandardised_data


def _preprocess_activations(activations, max_power=None, sample_period=6,
                            clip_appliance_power=False):
    for i, activation in enumerate(activations):
        # tz_convert(None) is a workaround for Pandas bug #5172
        # (AmbiguousTimeError: Cannot infer dst time from Timestamp)
        tz = activation.index.tz.zone
        activation = activation.tz_convert('UTC')
        freq = "{:d}S".format(sample_period)
        activation = activation.resample(freq)
        # activation.fillna(method='ffill', inplace=True)  # pandas 0.22.0 does not provide 'inplace' for DatetimeIndexResampler
        # activation.fillna(method='bfill', inplace=True)
        activation = activation.fillna(method='ffill')
        activation = activation.fillna(method='bfill')
        if clip_appliance_power:
            activation = activation.clip(0, max_power)
        activation = activation.tz_convert(tz)
        activations[i] = activation.astype(np.float32)
    return activations


"""
Emacs variables
Local Variables:
compile-command: "cp /home/jack/workspace/python/neuralnilm/neuralnilm_prototype/source.py /mnt/sshfs/imperial/workspace/python/neuralnilm/neuralnilm_prototype/"
End:
"""
