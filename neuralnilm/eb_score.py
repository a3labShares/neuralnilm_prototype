import numpy as np


def eb_score(y_true, y_pred):

    n = min(len(y_true), len(y_pred))
      
    y_true = y_true[:n]
    y_pred = y_pred[:n]

    sum_y_true = np.sum(y_true)
    sum_y_pred = np.sum(y_pred)

    flag = y_pred < y_true
    TP = np.sum(y_pred[flag]) + np.sum(y_true[~flag])

    try:
        eb_precision = float(TP/sum_y_pred)
    except Exception:
        eb_precision = 1
        
    try:    
    	eb_recall = float(TP/sum_y_true)
    except Exception:
        eb_recall = 1

    try:
        eb_f1_scores = float(2 * eb_precision * eb_recall / (eb_precision + eb_recall))
    except Exception:
	    eb_f1_scores =0

    return eb_f1_scores, eb_precision, eb_recall
    
    
def eb_f1_score(y_true, y_pred):
    
    eb_f1_scores, eb_precision, eb_recall = eb_score(y_true, y_pred)
    return eb_f1_scores